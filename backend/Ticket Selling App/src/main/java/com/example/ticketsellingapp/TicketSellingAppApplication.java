package com.example.ticketsellingapp;

import com.example.ticketsellingapp.models.AppUser;
import com.example.ticketsellingapp.models.Role;
import com.example.ticketsellingapp.service.AppUserService;
import com.example.ticketsellingapp.service.GenreService;
import com.example.ticketsellingapp.service.LocationService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class TicketSellingAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketSellingAppApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner run(AppUserService appUserService, GenreService genreService, LocationService locationService) {
        return args -> {
            appUserService.saveRole(new Role(null, "ROLE_USER"));

            genreService.saveNewGenre("Rock");
            genreService.saveNewGenre("Pop");
            genreService.saveNewGenre("Rap");

            locationService.saveNewLocation("Zagreb");
            locationService.saveNewLocation("Rijeka");
            locationService.saveNewLocation("Zadar");

            appUserService.saveAppUser(new AppUser(null, "Ivan", "ivan@gmail.com", "1111", new ArrayList<>()));
            appUserService.saveAppUser(new AppUser(null, "Luka", "luka@gmail.com", "2222", new ArrayList<>()));
            appUserService.saveAppUser(new AppUser(null, "Pero", "pero@gmail.com", "3333", new ArrayList<>()));
            appUserService.saveAppUser(new AppUser(null, "Perica", "perica@gmail.com", "4444", new ArrayList<>()));

            appUserService.addRoleToAppUser("ivan@gmail.com", "ROLE_USER");
            appUserService.addRoleToAppUser("luka@gmail.com", "ROLE_USER");
            appUserService.addRoleToAppUser("pero@gmail.com", "ROLE_USER");
            appUserService.addRoleToAppUser("perica@gmail.com", "ROLE_USER");

        };
    }

}
