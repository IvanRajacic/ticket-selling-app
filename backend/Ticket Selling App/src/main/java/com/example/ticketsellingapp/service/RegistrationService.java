package com.example.ticketsellingapp.service;

import com.example.ticketsellingapp.models.AppUser;
import com.example.ticketsellingapp.request.RegistrationRequest;
import com.example.ticketsellingapp.validation.EmailValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class RegistrationService {
    private final AppUserService appUserService;

    private final EmailValidator emailValidator;

    public String register(RegistrationRequest request) {
        boolean isValidEmail =
                emailValidator.test(request.getEmail());
        if (!isValidEmail) {
            throw new IllegalStateException("Email not valid.");
        }

        System.out.println("za sad radi");

        return appUserService.signUpUser(new AppUser(
                null,
                request.getName(),
                request.getEmail(),
                request.getPassword(),
                new ArrayList<>()
                )
        );
    }
}
