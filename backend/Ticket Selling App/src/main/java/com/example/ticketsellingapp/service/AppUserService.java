package com.example.ticketsellingapp.service;

import com.example.ticketsellingapp.models.AppUser;
import com.example.ticketsellingapp.models.Role;

import java.util.List;

public interface AppUserService {
    AppUser saveAppUser(AppUser appUser);
    Role saveRole(Role role);
    void addRoleToAppUser(String email, String roleName);
    AppUser getAppUser(Long appUserId, String tokenEmail);
    Long getAppUserId(String email, String tokenEmail);
    List<AppUser> getAppUsers();
    String signUpUser(AppUser appUser);

    void updateAppUser(Long appUserId, String name, String email,  String tokenEmail, String password);

}
