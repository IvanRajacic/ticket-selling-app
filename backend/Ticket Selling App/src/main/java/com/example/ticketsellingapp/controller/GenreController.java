package com.example.ticketsellingapp.controller;


import com.example.ticketsellingapp.models.Genre;
import com.example.ticketsellingapp.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @GetMapping("/allGenres")
    public ResponseEntity<List<Genre>> getAllGenres(){
        return ResponseEntity.ok().body(genreService.getAllGenres());
    }

}
