package com.example.ticketsellingapp.service.implementation;

import com.example.ticketsellingapp.models.Genre;
import com.example.ticketsellingapp.repository.GenreRepository;
import com.example.ticketsellingapp.service.GenreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;
    @Override
    public List<Genre> getAllGenres() {
        log.info("Fetching all genres");
        return genreRepository.findAll();
    }

    @Override
    public void saveNewGenre(String genre) {
        log.info("Saving new genre {} to the database", genre);
        genreRepository.save(new Genre(null, genre));
    }

    @Override
    public void deleteGenre(String genre) {
        if(genreRepository.findByName(genre) != null){
            log.error("Genre {} already exists!", genre);
            return;
        }
        log.info("Saving genre {} to the database", genre);
        genreRepository.delete(genreRepository.findByName(genre));
    }
}
