package com.example.ticketsellingapp.service;

import com.example.ticketsellingapp.models.Genre;
import com.example.ticketsellingapp.models.Location;

import java.util.List;

public interface LocationService {
    List<Location> getAllLocations();

    void saveNewLocation(String location);

    void deleteLocation(String location);
}
