package com.example.ticketsellingapp.service.implementation;

import com.example.ticketsellingapp.models.Genre;
import com.example.ticketsellingapp.models.Location;
import com.example.ticketsellingapp.repository.GenreRepository;
import com.example.ticketsellingapp.repository.LocationRepository;
import com.example.ticketsellingapp.service.LocationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;

    @Override
    public List<Location> getAllLocations() {
        log.info("Fetching all locations");
        return locationRepository.findAll();
    }

    @Override
    public void saveNewLocation(String location) {
        log.info("Saving new location {} to the database", location);
        locationRepository.save(new Location(null, location));
    }

    @Override
    public void deleteLocation(String location) {
        if (locationRepository.findByName(location) != null) {
            log.error("Location {} already exists!", location);
            return;
        }
        log.info("Saving location {} to the database", location);
        locationRepository.delete(locationRepository.findByName(location));
    }
}


