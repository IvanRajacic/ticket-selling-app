package com.example.ticketsellingapp.service;

import com.example.ticketsellingapp.models.Genre;

import java.util.List;

public interface GenreService {

    List<Genre> getAllGenres();

    void saveNewGenre(String genre);

    void deleteGenre(String genre);
}
