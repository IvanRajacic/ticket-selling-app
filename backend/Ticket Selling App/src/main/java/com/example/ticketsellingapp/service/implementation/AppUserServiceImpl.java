package com.example.ticketsellingapp.service.implementation;

import com.example.ticketsellingapp.models.AppUser;
import com.example.ticketsellingapp.models.Role;
import com.example.ticketsellingapp.repository.AppUserRepository;
import com.example.ticketsellingapp.repository.RoleRepository;
import com.example.ticketsellingapp.service.AppUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AppUserServiceImpl implements AppUserService, UserDetailsService {

    private final AppUserRepository appUserRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AppUser appUser = appUserRepository.findByEmail(email);
        if (appUser == null) {
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            log.info("User found in the database: {}", email);
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        appUser.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(appUser.getEmail(), appUser.getPassword(), authorities);
    }

    @Override
    public AppUser saveAppUser(AppUser appUser) {
        log.info("Saving new appUser {} to the database", appUser.getName());
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        return appUserRepository.save(appUser);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Saving new role {} to the database", role.getName());

        return roleRepository.save(role);
    }

    @Override
    public void addRoleToAppUser(String email, String roleName) {
        log.info("Adding role {} to appUser {} to the database", roleName, email);

        AppUser appUser = appUserRepository.findByEmail(email);
        Role role = roleRepository.findByName(roleName);
        appUser.getRoles().add(role);

    }

    @Override
    public AppUser getAppUser(Long appUserId, String tokenEmail) {
        AppUser appUser = appUserRepository.findById(appUserId)
                .orElseThrow(() -> new IllegalStateException(
                                "Student with id " + appUserId + "does not exist."
                        )
                );

        //provjerava jeli user zapravo user
        if (!appUser.getEmail().equals(tokenEmail)) {
            throw new org.springframework.security.access.AccessDeniedException("403 returned");
        }

        log.info("Fetching appUser with email: {} to the database", appUser.getEmail());
        return appUser;
    }

    @Override
    public Long getAppUserId(String email, String tokenEmail){

        if (!email.equals(tokenEmail)) {
            throw new org.springframework.security.access.AccessDeniedException("403 returned");
        }

        return appUserRepository.findByEmail(email).getId();
    }

    @Override
    public List<AppUser> getAppUsers() {
        log.info("Fetching all appUsers");
        return appUserRepository.findAll();
    }

    @Override
    public String signUpUser(AppUser appUser) {
        boolean userExists = appUserRepository.findByEmail(appUser.getEmail()) != null;

        if (userExists) {
            throw new IllegalStateException("Email already taken");
        }

        saveAppUser(appUser);

        addRoleToAppUser(appUser.getEmail(), "ROLE_USER");

        //TODO sent confirmation token

        return "it works";
    }

    @Override
    @Transactional
    public void updateAppUser(Long appUserId,  String name, String email,  String tokenEmail, String password) {
        AppUser appUser = appUserRepository.findById(appUserId)
                .orElseThrow(() -> new IllegalStateException(
                                "Student with id " + appUserId + "does not exist."
                        )
                );
        //provjerava jeli user zapravo user
        if (!appUser.getEmail().equals(tokenEmail)) {
            throw new org.springframework.security.access.AccessDeniedException("403 returned");
        }

        if (name != null &&
                name.length() > 0 &&
                !Objects.equals(appUser.getName(), name)) {
            appUser.setName(name);
            System.out.printf("name changed");
        }

        if (email != null &&
                email.length() > 0 &&
                !Objects.equals(appUser.getEmail(), email)) {
            if (appUserRepository.findByEmail(email) == null) {
                System.out.println(email);
                appUser.setEmail(email);
                System.out.printf("email changed");

            }
        }
        if (password != null) {
            password = passwordEncoder.encode(password);
        }
        if (password != null &&
                password.length() > 0 &&
                !Objects.equals(appUser.getPassword(), password)) {
            appUser.setPassword(password);
            System.out.printf("password changed");

        }
    }
}
