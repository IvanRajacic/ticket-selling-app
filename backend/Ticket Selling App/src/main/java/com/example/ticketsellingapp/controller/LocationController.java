package com.example.ticketsellingapp.controller;

import com.example.ticketsellingapp.models.Genre;
import com.example.ticketsellingapp.models.Location;
import com.example.ticketsellingapp.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class LocationController {

    private final LocationService locationService;

    @GetMapping("/allLocations")
    public ResponseEntity<List<Location>> getAllLocations(){
        return ResponseEntity.ok().body(locationService.getAllLocations());
    }

}
