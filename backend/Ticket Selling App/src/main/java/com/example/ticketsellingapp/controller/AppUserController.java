package com.example.ticketsellingapp.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.ticketsellingapp.models.AppUser;
import com.example.ticketsellingapp.models.Role;
import com.example.ticketsellingapp.service.AppUserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AppUserController {
    private final AppUserService appUserService;


    @GetMapping("/appUsers")
    public ResponseEntity<List<AppUser>> getAppUsers() {
        return ResponseEntity.ok().body(appUserService.getAppUsers());
    }

    @GetMapping("/appUser/{appUserId}")
    public ResponseEntity<AppUser> getAppUser(
            @PathVariable("appUserId") Long appUserId,
            @RequestHeader("Authorization") String authorizationHeader
    ) {
        String token = authorizationHeader.substring("Bearer ".length());
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        String tokenEmail = decodedJWT.getSubject();

        return ResponseEntity.ok().body(appUserService.getAppUser(appUserId, tokenEmail));
    }

    @GetMapping("/appUserId/{email}")
    public ResponseEntity<Long> getAppUser(
            @PathVariable("email") String email,
            @RequestHeader("Authorization") String authorizationHeader
    ) {
        String token = authorizationHeader.substring("Bearer ".length());
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        String tokenEmail = decodedJWT.getSubject();

        return ResponseEntity.ok().body(appUserService.getAppUserId(email, tokenEmail));
    }



    @PostMapping("/appUser/save")
    public ResponseEntity<AppUser> saveAppUser(@RequestBody AppUser appUser) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/appUser/save").toUriString());
        return ResponseEntity.created(uri).body(appUserService.saveAppUser(appUser));
    }

    @PostMapping("/role/save")
    public ResponseEntity<Role> saveRole(@RequestBody Role role) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
        return ResponseEntity.created(uri).body(appUserService.saveRole(role));
    }

    @PostMapping("/role/addRoleToAppUser")
    public ResponseEntity<?> addRoleToAppUser(@RequestBody RoleToAppUserForm form) {
        appUserService.addRoleToAppUser(form.getEmail(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

//    @PostMapping("/genre/addGenreToAppUser")
//    public ResponseEntity<?> addGenreToAppUser(@RequestBody RoleToAppUserForm form) {
//        appUserService.addRoleToAppUser(form.getEmail(), form.getRoleName());
//        return ResponseEntity.ok().build();
//    }
//
//    @PostMapping("/location/changeUserLocation")
//    public ResponseEntity<?> addLocationToAppUser(@RequestBody RoleToAppUserForm form) {
//        appUserService.addRoleToAppUser(form.getEmail(), form.getRoleName());
//        return ResponseEntity.ok().build();
//    }

    @PutMapping(path = "/{appUserId}")
    public void updateAppUser(
            @PathVariable("appUserId") Long appUserId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String password,
            @RequestHeader("Authorization") String authorizationHeader
    ) {
        String token = authorizationHeader.substring("Bearer ".length());
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        String tokenEmail = decodedJWT.getSubject();
        appUserService.updateAppUser(appUserId, name, email, tokenEmail, password);

    }
}

@Data
class RoleToAppUserForm {
    private String email;
    private String roleName;
}

