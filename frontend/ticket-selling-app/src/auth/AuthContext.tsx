import * as React from 'react';
import { IAuthProviderProps } from './IAuthProviderProps';

export const AuthContext = React.createContext<IAuthProviderProps>(
    {} as IAuthProviderProps
);

interface Props {
    children: React.ReactNode | React.ReactNode[];
}

export const AuthProvider = ({ children }: Props): JSX.Element => {
    const [userLoggedIn, setUserLogedIn] = React.useState<boolean>();
    const [userId, setUserId] = React.useState<string>();

    /* Define provider value. Can be done inline, but I've defined
    it here for clarity */
    const providerValue: IAuthProviderProps = {
        userLoggedIn,
        setUserLogedIn,
        userId,
        setUserId
    };

    /* Pass providerValue to provider */
    return (
        <AuthContext.Provider value={providerValue}>
            {children}
        </AuthContext.Provider>
    );
};
