import { Dispatch, SetStateAction } from 'react';
import internal from 'stream';

export interface IAuthProviderProps {
    userLoggedIn?: boolean;
    setUserLogedIn: Dispatch<SetStateAction<boolean | undefined>>;
    userId?: string;
    setUserId: Dispatch<SetStateAction<string | undefined>>;
}
