import * as React from 'react';

import './Header.css';

import { FaTicketAlt } from 'react-icons/fa';
import { Link, NavLink } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';

const Header = () => {
    const { userLoggedIn } = React.useContext(AuthContext);

    return (
        <div className="header-container">
            <div className="header-logo-container">
                <NavLink className="link" to="/">
                    <FaTicketAlt className="page-icon" />
                </NavLink>
            </div>
            {localStorage.getItem('token') || userLoggedIn ? (
                <div className="header-menu-container">
                    <div className="header-menu-item">
                        <NavLink className="link" to="/profile">
                            Profile
                        </NavLink>
                    </div>
                    <div className="header-menu-item">
                        <div
                            className="header-logout"
                            onClick={() => {
                                localStorage.clear();
                                window.location.reload();
                            }}
                        >
                            Logout
                        </div>
                    </div>
                </div>
            ) : (
                <div className="header-menu-container">
                    <div className="header-menu-item">
                        <NavLink className="link" to="/login">
                            Login
                        </NavLink>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Header;
