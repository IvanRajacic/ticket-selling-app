//imports
import * as React from 'react';
import axios from 'axios';
import qs from 'qs';
import { NavLink, useNavigate } from 'react-router-dom';
//material-ui
import { Box, Button, TextField } from '@mui/material';
//styles
import './Login.css';
import { AuthContext } from '../../auth/AuthContext';

const Login = () => {
    const { setUserLogedIn, setUserId } = React.useContext(AuthContext);
    let navigate = useNavigate();
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const [apiError, setApiError] = React.useState('');

    function handleSumbit() {
        setError('');
        if (email === '' || password === '') {
            setError('Please fill all the fields');
        }
        let data = qs.stringify({
            email: email,
            password: password
        });
        axios
            .post('http://localhost:8080/api/login', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then((response) => {
                localStorage.setItem('token', response.data.access_token);
                axios
                    .get('http://localhost:8080/api/appUserId/' + email, {
                        headers: {
                            Authorization:
                                'Bearer ' + response.data.access_token
                        }
                    })
                    .then((response) => {
                        localStorage.setItem('userId', response.data);
                        setUserLogedIn(true);
                    });
                navigate('/');
            })
            .catch((error) => {
                setError('Wrong username or password');
            });
    }

    return (
        <div className="login">
            <div className="main-login-container">
                <div className="title">Login</div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        label="E-mail"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your e-mail"
                        error={error !== ''}
                        helperText={error}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                </div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        type="password"
                        label="Password"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your password"
                        error={error !== ''}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </div>
                <Button
                    variant="contained"
                    size="large"
                    className="login-btn"
                    sx={{ height: 50, mt: 2 }}
                    onClick={() => handleSumbit()}
                >
                    Login
                </Button>
                <div className="register-container">
                    <Box>Don't have an account?</Box>
                    <NavLink to="/register">
                        <Button sx={{ height: 35 }}>Sign up</Button>
                    </NavLink>
                </div>
            </div>
        </div>
    );
};

export default Login;
