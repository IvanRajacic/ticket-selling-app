import * as React from 'react';
import axios from 'axios';
import qs from 'qs';
import { useNavigate } from 'react-router-dom';
//material-ui
import { Button, TextField } from '@mui/material';
//styles
import './Profile.css';
import { AuthContext } from '../../auth/AuthContext';

const Profile = () => {
    const { setUserLogedIn } = React.useContext(AuthContext);
    let navigate = useNavigate();
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [oldEmail, setoldEmail] = React.useState('');

    const [password, setPassword] = React.useState('');
    // const [repeatPassword, setRepeatPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const [emailError, setEmailError] = React.useState('');
    // const [passwordError, setPasswordError] = React.useState('');
    const [apiError, setApiError] = React.useState('');
    // const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        axios
            .get(
                'http://localhost:8080/api/appUser/' +
                    localStorage.getItem('userId'),
                {
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('token')
                    }
                }
            )
            .then((response) => {
                setName(response.data.name);
                setEmail(response.data.email);
                setoldEmail(response.data.email);
                setPassword(response.data.password);
            })
            .catch((error) => {
                setApiError('Error while getting user data');
            });
    }, []);

    function handleSumbit() {
        setError('');
        setApiError('');
        if (name == '' || email === '') {
            setError('Please fill all the fields');
            return;
        }
        if (!email.includes('@')) {
            setEmailError('Please enter a valid email');
            return;
        }

        //prvo gledamo je li user tocan
        let data = qs.stringify({
            email: oldEmail,
            password: password
        });
        axios
            .post('http://localhost:8080/api/login', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then((response) => {})
            .catch((error) => {
                setApiError('Wrong password');
                return;
            });

        //ako je onda posalji put request

        data = JSON.stringify({
            name: name,
            email: email
        });
        axios
            .put(
                'http://localhost:8080/api/' +
                    localStorage.getItem('userId') +
                    '?' +
                    'name=' +
                    name +
                    '&email=' +
                    email,
                data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + localStorage.getItem('token')
                    }
                }
            )
            .then((response) => {
                handleAccessToken(email, password, setUserLogedIn);
            })
            .catch((error) => {
                setApiError('Something went wrong');
            });
    }

    return (
        <div className="login">
            <div className="main-login-container">
                <div className="title">Your profile</div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your first name"
                        value={name}
                        error={error !== ''}
                        helperText={error}
                        onChange={(event) => setName(event.target.value)}
                    />
                </div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        label="E-mail"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your e-mail"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                </div>
                <div className="password-entry-container">
                    <div className="password-entry-text">
                        Enter you password to confirm identity and change data
                    </div>
                    <TextField
                        id="outlined-basic"
                        type="password"
                        label="Password"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your password"
                        error={error !== ''}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </div>
                <div className="error-container">{apiError}</div>
                <Button
                    variant="contained"
                    size="large"
                    className="change-your-profile-btn"
                    sx={{ height: 50, mt: 2 }}
                    onClick={() => handleSumbit()}
                >
                    Change your profile
                </Button>
            </div>
        </div>
    );
};

export default Profile;

function handleAccessToken(
    email: string,
    password: string,
    setUserLogedIn: React.Dispatch<React.SetStateAction<boolean | undefined>>
) {
    let data = qs.stringify({
        email: email,
        password: password
    });

    axios
        .post('http://localhost:8080/api/login', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then((response) => {
            localStorage.setItem('token', response.data.access_token);
            setUserLogedIn(true);
        });
}
