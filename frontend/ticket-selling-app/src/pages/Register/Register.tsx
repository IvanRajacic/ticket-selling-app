//imports
import * as React from 'react';
import axios from 'axios';
import qs from 'qs';
import { NavLink, useNavigate } from 'react-router-dom';
//material-ui
import { Box, Button, TextField } from '@mui/material';
//styles
import './Register.css';
import { AuthContext } from '../../auth/AuthContext';

const Register = () => {
    const { setUserLogedIn } = React.useContext(AuthContext);
    let navigate = useNavigate();
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [repeatPassword, setRepeatPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const [emailError, setEmailError] = React.useState('');
    const [passwordError, setPasswordError] = React.useState('');
    const [apiError, setApiError] = React.useState('');

    function handleSumbit() {
        setError('');
        setPasswordError('');
        setApiError('');
        if (
            name == '' ||
            email === '' ||
            password === '' ||
            repeatPassword === ''
        ) {
            setError('Please fill all the fields');
            return;
        }
        if (password !== repeatPassword) {
            setPasswordError('Passwords do not match');
            return;
        }
        if (!email.includes('@')) {
            setEmailError('Please enter a valid email');
            return;
        }

        let data = JSON.stringify({
            name: name,
            email: email,
            password: password
        });
        axios
            .post('http://localhost:8080/api/registration', data, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                handleAccessToken(email, password, setUserLogedIn);
                navigate('/');
            })
            .catch((error) => {
                setApiError('Something went wrong');
            });
    }

    return (
        <div className="login">
            <div className="main-login-container">
                <div className="title">Register</div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your first name"
                        error={error !== ''}
                        helperText={error}
                        onChange={(event) => setName(event.target.value)}
                    />
                </div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        label="E-mail"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your e-mail"
                        error={error !== '' || emailError !== ''}
                        helperText={emailError}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                </div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        type="password"
                        label="Password"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Enter your password"
                        error={error !== '' || passwordError !== ''}
                        helperText={passwordError}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </div>
                <div className="login-container">
                    <TextField
                        id="outlined-basic"
                        type="password"
                        label="Repeat password"
                        variant="outlined"
                        className="input-field"
                        fullWidth
                        placeholder="Repeat your password"
                        error={error !== '' || passwordError !== ''}
                        onChange={(event) =>
                            setRepeatPassword(event.target.value)
                        }
                    />
                </div>
                <div className="error-container">{apiError}</div>
                <Button
                    variant="contained"
                    size="large"
                    className="login-btn"
                    sx={{ height: 50, mt: 2 }}
                    onClick={() => handleSumbit()}
                >
                    Register
                </Button>
            </div>
        </div>
    );
};

export default Register;

function handleAccessToken(
    email: string,
    password: string,
    setUserLogedIn: React.Dispatch<React.SetStateAction<boolean | undefined>>
) {
    let data = qs.stringify({
        email: email,
        password: password
    });
    axios
        .post('http://localhost:8080/api/login', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then((response) => {
            localStorage.setItem('token', response.data.access_token);
            setUserLogedIn(true);
            axios
                .get('http://localhost:8080/api/appUser/' + email, {
                    headers: {
                        Authorization: 'Bearer ' + response.data.access_token
                    }
                })
                .then((response) => {
                    localStorage.setItem('userId', response.data);
                    setUserLogedIn(true);
                });
        });
}
