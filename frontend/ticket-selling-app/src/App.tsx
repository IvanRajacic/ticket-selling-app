import './App.css';

import * as React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/Home/HomePage';
import Login from './pages/Login/Login';
import Header from './components/Header/Header';
import Register from './pages/Register/Register';
import { createTheme } from '@mui/material';
import { ThemeProvider } from '@emotion/react';
import { AuthContext, AuthProvider } from './auth/AuthContext';
import Profile from './pages/Profile/Profile';
const theme = createTheme({
    palette: {
        primary: {
            main: '#3cc47c',
            contrastText: '#fff'
        }
    }
});

function App() {
    const [authenticated, setAuthenticated] = React.useState<boolean>(false);
    return (
        <AuthProvider>
            <ThemeProvider theme={theme}>
                <div className="App">
                    <BrowserRouter>
                        <Header />
                        <Routes>
                            <Route path="/" element={<HomePage />} />
                            <Route path="/login" element={<Login />} />
                            <Route path="/register" element={<Register />} />
                            <Route path="/profile" element={<Profile />} />
                        </Routes>
                    </BrowserRouter>
                </div>
            </ThemeProvider>
        </AuthProvider>
    );
}

export default App;
